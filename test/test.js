describe('finishDraw', () => {
    let sandbox = null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should call updateParameters', function () {
        const event = {
            clientX: 5,
            clientY: 5,
        };
        const stub = sandbox.stub(window, 'updateParameters');

        finishDraw(event);

        sandbox.assert.calledOnce(stub);
    });

    it('should call startFunc', function () {
        const event = {
            clientX: 5,
            clientY: 5,
        };
        targetValue = "Rectangle";
        const stub = sandbox.stub(window, 'startFunc');

        finishDraw(event);

        sandbox.assert.calledOnce(stub);
        sandbox.assert.calledWith(stub, targetValue);
    });
});

describe('startFunc', () => {
    let sandbox = null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should call drawRect if pressed Rectangle button', () => {
        targetValue = "Rectangle";
        const stub = sandbox.stub(window, 'drawRect');

        startFunc(targetValue);

        sandbox.assert.calledOnce(stub);
    });

    it('should call drawRoundRect if pressed Rounded Rectangle button', () => {
        targetValue = "Rounded Rectangle";
        const stub = sandbox.stub(window, 'drawRoundRect');

        startFunc(targetValue);

        sandbox.assert.calledOnce(stub);
    });

    it('should call drawLine if pressed Line button', () => {
        targetValue = "Line";
        const stub = sandbox.stub(window, 'drawLine');

        startFunc(targetValue);

        sandbox.assert.calledOnce(stub);
    });

    it('should call drawOval if pressed Oval button', () => {
        targetValue = "Oval";
        const stub = sandbox.stub(window, 'drawOval');

        startFunc(targetValue);

        sandbox.assert.calledOnce(stub);
    });

    it('should call drawEllipse if pressed Ellipse button', () => {
        targetValue = "Ellipse";
        const stub = sandbox.stub(window, 'drawEllipse');

        startFunc(targetValue);

        sandbox.assert.calledOnce(stub);
    });
});

