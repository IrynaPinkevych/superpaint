const canvas = document.getElementById('canvas');
const c = canvas.getContext('2d');
const inputs = document.querySelectorAll('.buttons');
const inputColor = document.getElementById("pickColor");
const inputWeight = document.getElementById('pickLine');

let x1, x2, y1, y2;
let targetValue;

for (let i = 1; i < inputs.length; i++) {
    let temp = inputs[i];

    temp.addEventListener('click', function (event) {
        targetValue = event.target.value;
    });
}

canvas.addEventListener('mousedown', function (event) {
    x1 = event.clientX;
    y1 = event.clientY;
});

canvas.addEventListener('mouseup', function (event) {
    x2 = event.clientX;
    y2 = event.clientY;
    updateParameters();
    startFunc(targetValue);
});

const startFunc = (targetValue) => {

    switch (targetValue) {
        case "Rectangle":
            drawRect(x1, y1, x2, y2);
            break;

        case "Rounded Rectangle":
            drawRoundRect(x1, y1, x2, y2);
            break;

        case "Line":
            drawLine(x1, y1, x2, y2);
            break;

        case "Oval":
            drawOval(x1, y1, x2, y2);
            break;

        case "Ellipse":
            drawEllipse(x1, y1, x2, y2);
            break;

        default:
            break;
    }

};

const updateParameters = () =>{
    c.strokeStyle = inputColor.value;
    c.lineWidth = inputWeight.value;
};

// прямоугольник по координатам
// (x1;y1) - первый угол (x2;y2) - противоположный угол
const drawRect = (x1, y1, x2, y2) => {
    c.strokeRect(x1, y1, x2 - x1, y2 - y1);
    c.stroke();
};

// прямоугольник с округленными углами по координатам
// (x1;y1) - первый угол (x2;y2) - противоположный угол
const drawRoundRect = (x1, y1, x2, y2) => {
    const radius = 10;

    if (x2 < x1){//если блок выделяется снизу вверх или справа на лево, что бы углы не выкручивало
        let x = x1; x1 = x2; x2 = x;
    }
    if (y2 < y1){
        let y = y1; y1 = y2; y2 = y;
    }

    c.beginPath();
    c.moveTo(x1, y1 + radius);
    c.lineTo(x1, y2 - radius);
    c.quadraticCurveTo(x1, y2, x1 + radius, y2);
    c.lineTo(x2 - radius, y2);
    c.quadraticCurveTo(x2, y2, x2, y2 - radius);
    c.lineTo(x2, y1 + radius);
    c.quadraticCurveTo(x2, y1, x2 - radius, y1);
    c.lineTo(x1 + radius, y1);
    c.quadraticCurveTo(x1, y1, x1, y1 + radius);
    c.stroke();
};

const drawLine = (x1, y1, x2, y2) => {

    c.beginPath();
    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.stroke();
};

const drawOval = (x1, y1, x2, y2) => {

    let xMid = (x2 + x1) / 2;
    let yMid = (y2 + y1) / 2;
    c.beginPath();
    c.moveTo(x1, yMid);
    c.quadraticCurveTo(x1, y1, xMid, y1);
    c.quadraticCurveTo(x2, y1, x2, yMid);
    c.quadraticCurveTo(x2, y2, xMid, y2);
    c.quadraticCurveTo(x1, y2, x1, yMid);
    c.stroke();
};

// рисуем эллипс
const drawEllipse = (x1, y1, x2, y2) => {
    if (x2 < x1){ //если блок выделяется снизу вверх или справа на лево, что бы углы не выкручивало
        let x = x1; x1 = x2; x2 = x;
    }
    if (y2 < y1){
        let y = y1; y1 = y2; y2 = y;
    }

    c.save(); // Запоминаем положение системы координат (CК) и масштаб
    c.beginPath();
    // находим полуоси
    let a = (x2 - x1) / 2;
    let b = (y2 - y1) / 2;

    c.translate((x1 + x2) / 2, (y1 + y2) / 2); // Переносим СК в центр будущего эллипса
    c.scale(a / b, 1); // Масштабируем по х.

    // Рисуем окружность, которая благодаря масштабированию станет эллипсом
    c.arc(0, 0, b, 0, Math.PI * 2, true);

    c.restore(); // Восстанавливаем СК и масштаб

    c.closePath();
    c.stroke();
};
